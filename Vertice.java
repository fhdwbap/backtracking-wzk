/**
 * Package backtracking :  
 * Demonstration zur Vererbung: Klassen Person, Student, Dozent
 * fhdwbap/2019
 */
package backtracking;

/**
 * Modellierung eines Knotens im Graphen
 * @author fhdwbap
 *
 */
public class Vertice 
{
    private static int vcounter = 0;  // Zähler für alle Vertice-Objekte 
    
	private int number = 0;  // interne Nummer 
	private String label = "xxxx"; // 4-stellige Kennzeichnung
	private Vertice[] neighbour = new Vertice[BTMain.MAXVERTICES];

	/*
	 * Default-Konstruktor von Vertice
	 */
	public Vertice()
	{
	    this.number = ++vcounter;
	    
		// Standard-Knoten
		for (int i=0; i<neighbour.length; i++)
		{
			neighbour[i] = null;
		}
		
	}
	
	/*
	 * Konstruktor mit der Label-Information von außen
	 */
	public Vertice(String label)
	{
		this();
		this.label = label;
	}
	
	/*
	 * Konstruktor mit der numerischen (dezimalen) Information für das
	 * Label von außen
	 */	
	public Vertice(int nr)
	{
		this();
		this.label = binaryAsString(nr);
		
		// Wir wollen stets vier Bits ("0000", "0001" etc.) als Label verwenden! 
		while (this.label.length() < 4)
		{
			this.label = "0" + this.label;
		}
	}
	
	/**
	 * Ausgabe eines Knoten inklusive seiner Nachfolger
	 */
	public void show()
	{
		System.out.println("Knoten (" + this.label + ") [#" + number + "].");
		
		for (int i=0; i<neighbour.length; i++)
		{
			if (neighbour[i] == null)
			{
				break;
			}
			System.out.print("     +---> Nachfolger: ");
			neighbour[i].showFlat();
		}
	}
	
	/*
	 * "Flache" Ausgabe eines Knoten (ohne d. eventuellen Nachfolger)
	 */
	public void showFlat()
	{
		System.out.println("Knoten (" + this.label + ") [#" + number + "].");
	
	}
	
	/*
	 * String-Darstellung eines Knoten
	 */
	@Override
	public String toString()
	{
		return "Vertice #" + number + " Label: " + label;
	}
	
	/*
	 * Setzen einer Verbindung (Edge, Kante)
	 */
	public void setNeighbour(int lfd, Vertice v)
	{
		neighbour[lfd] = v;
	}
	
	/*
	 * Abruf des Arrays der Nachbar-Knoten
	 */
	public Vertice[] getNeighbour()
	{
		return neighbour;
	}
	
	/*
	 * Abruf der Anzahl der Nachbar-Knoten
	 */
	public int numberOfNeighbours()
	{
		int number = 0;
		for (int i=0; i<neighbour.length; i++)
		{
			if (neighbour[i] == null)
			{
				break;
			}
			number++;
		}
		return number;		
	}
	
	/*
	 * Intern: Anzeige der Anzahl der Nachbar-Knoten
	 * (Wäre auch via numberOfNeighbours() zu realisieren)
	 */
	private void showNumberOfNeighbours()
	{
		int numberOfNeighbours = 0;
		for (int i=0; i<neighbour.length; i++)
		{
			if (neighbour[i] == null)
			{
				break;
			}
			numberOfNeighbours++;
		}
		System.out.println("Knoten hat " + numberOfNeighbours + " Nachbarn/Verbindungen.");		
	}
	
	/*
	 * Darstellung einer nichtnegativen ganzen Zahl als Zeichenkette.<br/>
	 * Methode aus Kompatibilität zur C-Version dieses Programms; in Java 
	 * kann direkt die statische toBinaryString()-Methode der Wrapper-Klasse
	 * Integer verwendet werden. 
	 */
	private String binaryAsString(int zahl)
	{
		return Integer.toBinaryString(zahl);
	}	
	
	
} // end class Vertice 


